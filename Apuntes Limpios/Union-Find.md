# ¿Que es una Union Find?:
> Union-Find data structure es una estrucutra de datos que almacena una colección de conjuntos disjntos. El pilar de ello es representar el un arbol con dos array, donde uno contiene el etiquetado de todos los nodos con los que vamos a trabajar, y el otro, tiene los padres de los nodos que están contenidos en el array previamente descrito.
>
>![alt text](./../Img/disjointset.png "Diseño Inicial")
>

> ## ¿Que tiene esto de Find y Union?
> Este sistema de de clasificación permite que con una simple función de recursión podemos dar con el padre de un grafo con la condición `i = parent[i]`. Teniendo claro quienes son los padres de cada arbol, podemos arbitrariamente unir un padre con el otro, teniendo así un solo grafo.
