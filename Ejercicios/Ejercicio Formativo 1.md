# **Pregunta 1**

<br>
<br>

## (a) *¿Cual es el orden de complejidad del algoritmo de Prim en el peor caso para n y m fijado?*
---

<br>

> En el peor escenario, **dado que no hay orden garantizado**, por cada nodo es necesario buscar a través sus vecinos. Dado que estamos en el peor caso, buscar el mínimo de ellos es de orden $O(n)$ y considerando que tenemos $n$ nodos, aplicar esto sobre todo el grafo sería de orden $n \times O(n)$ o equivalentemente: $O(n^2)$

## (b) *¿Se puede mejorar el algoritmo de Prim con una lista ordenada de arcos?*
---

<br>

> Depende del contexto. Dados los videos de Poblete, sabemos que:
> - Una lista ordenada puede actuar como una cola de prioridad
> - Una cola de prioridad se puede implementar con heaps
>
> Todo lo anterior implica que poodemos asumir a traves de dichos videos que el algoritmo de Prim con listas ordenadas es del orden $O(m * log (n))$. Por la
> pregunta anterior, sabemos que el peor caso del algoritmo, es de orden $O(n^2)$, de modo que si el grafo contiene muchas aristas, conviene no usar listas ordenadas, y si tiene pocas, entonces conviene usar las listas ordenadas.
>
> Ejemplo:
> | $n$  |  $m$  | $m * log (n)$ | $n^2$ |
> | :--- | :---: | :-----------: | ----: |
> | 100  |  100  |    664.38     | 10000 |
> | 100  | 2000  |   13287.71    | 10000 |


<br>

## (c) *¿Se puede mejorar el algoritmo de Prim con una estructura de tipo Union Find?*
---

<br>

> Por como yo lo entiendo, la estructura de *Union Find* permite unir subgrafos disjuntos, pero el tema con Prim es que uno se ubica en un vértice aleatorio y comienza a conectar el grafo con los **vecinos de dicho vértice**, por lo cual tener una estructura como esta no tendría mucho sentido ya que en principio el escenario en donde Union Find es útil, nunca ocurrirá, a diferencia de prim, en la cual si se generan grafos disjuntos durante la busqueda el MSP.
> 
> En sisntesis, no se podría mejorar sin cambiar radicalmente como opera el principio base del algoritmo.
